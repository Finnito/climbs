<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Rating\Form\RatingFormBuilder;
use Finnito\ClimbsModule\Rating\Table\RatingTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class RatingsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param RatingTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(RatingTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param RatingFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(RatingFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param RatingFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(RatingFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
