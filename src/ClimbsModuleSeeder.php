<?php namespace Finnito\ClimbsModule;

use Finnito\ClimbsModule\Country\CountrySeeder;
use Finnito\ClimbsModule\ClimbingGrade\ClimbingGradeSeeder;
use Finnito\ClimbsModule\Style\StyleSeeder;
use Finnito\ClimbsModule\Rock\RockSeeder;
use Finnito\ClimbsModule\Protection\ProtectionSeeder;
use Finnito\ClimbsModule\Type\TypeSeeder;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class ClimbsModuleSeeder
 *
 * @link   
 * @author 
 * @author 
 */
class ClimbsModuleSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->call(CountrySeeder::class);
        $this->call(ClimbingGradeSeeder::class);
        $this->call(StyleSeeder::class);
        $this->call(RockSeeder::class);
        $this->call(ProtectionSeeder::class);
        $this->call(TypeSeeder::class);
    }
}
