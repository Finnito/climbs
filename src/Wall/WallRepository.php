<?php namespace Finnito\ClimbsModule\Wall;

use Finnito\ClimbsModule\Wall\Contract\WallRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class WallRepository extends EntryRepository implements WallRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var WallModel
     */
    protected $model;

    /**
     * Create a new WallRepository instance.
     *
     * @param WallModel $model
     */
    public function __construct(WallModel $model)
    {
        $this->model = $model;
    }
}
