<?php namespace Finnito\ClimbsModule\Rock;

use Finnito\ClimbsModule\Rock\Contract\RockRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class RockRepository extends EntryRepository implements RockRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var RockModel
     */
    protected $model;

    /**
     * Create a new RockRepository instance.
     *
     * @param RockModel $model
     */
    public function __construct(RockModel $model)
    {
        $this->model = $model;
    }
}
