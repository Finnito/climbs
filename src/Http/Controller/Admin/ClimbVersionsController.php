<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Climb\ClimbModel;

class ClimbVersionsController extends \Anomaly\Streams\Platform\Http\Controller\VersionsController
{

   /**
     * The versionable model.
     *
     * @var string
     */
    protected $model = ClimbModel::class;

}