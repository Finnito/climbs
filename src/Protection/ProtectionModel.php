<?php namespace Finnito\ClimbsModule\Protection;

use Finnito\ClimbsModule\Protection\Contract\ProtectionInterface;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsProtectionEntryModel;

class ProtectionModel extends ClimbsProtectionEntryModel implements ProtectionInterface
{

}
