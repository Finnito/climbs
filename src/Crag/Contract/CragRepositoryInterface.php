<?php namespace Finnito\ClimbsModule\Crag\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CragRepositoryInterface extends EntryRepositoryInterface
{

}
