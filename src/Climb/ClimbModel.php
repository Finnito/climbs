<?php namespace Finnito\ClimbsModule\Climb;

use Finnito\ClimbsModule\Climb\Contract\ClimbInterface;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsClimbsEntryModel;
use Finnito\ClimbsModule\Comment\CommentModel;
use Finnito\ClimbsModule\Send\SendModel;
use Finnito\ClimbsModule\Rating\RatingModel;

class ClimbModel extends ClimbsClimbsEntryModel implements ClimbInterface
{
    /**
     * Return the comments on the climb
     *
     * @return null|CommentCollection
     */
    public function getComments()
    {
        return $this->hasMany(
            CommentModel::class,
            "climb_id"
        )
        ->orderBy("created_at", "DESC")
        ->get();
    }

    /**
     * Return the sends for the climb
     *
     * @return null|SendCollection
     */
    public function getSends()
    {
        return $this->hasMany(
            SendModel::class,
            "climb_id"
        )
        ->orderBy("created_at", "DESC")
        ->get();
    }

    /**
     * Return the rating for the climb
     *
     * @return null|double
     */
    public function getRating()
    {
        $average = $this->hasMany(
            RatingModel::class,
            "climb_id"
        )
        ->avg("rating");

        return round($average);
    }
}
