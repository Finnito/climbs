<?php

return [
    'new_country' => 'New Country',
    'new_climbing_grade' => 'New Climbing Grade',
    'new_crag' => 'New Crag',
    'new_style' => 'New Style',
    'new_wall' => 'New Wall',
    'new_rock' => 'New Rock',
    'new_protection' => 'New Protection',
    'new_type' => 'New Type',
    'new_rating' => 'New Rating',
    'new_climb' => 'New Climb',
    'new_send' => 'New Send',
    'new_comment' => 'New Comment',
];
