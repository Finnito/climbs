<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Protection\Form\ProtectionFormBuilder;
use Finnito\ClimbsModule\Protection\Table\ProtectionTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ProtectionController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ProtectionTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ProtectionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ProtectionFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ProtectionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ProtectionFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ProtectionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
