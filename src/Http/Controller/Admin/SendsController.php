<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Send\Form\SendFormBuilder;
use Finnito\ClimbsModule\Send\Table\SendTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class SendsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param SendTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SendTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param SendFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(SendFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param SendFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(SendFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
