<?php namespace Finnito\ClimbsModule\Protection;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Finnito\ClimbsModule\Protection\Contract\ProtectionRepositoryInterface;

class ProtectionSeeder extends Seeder
{

    /**
     * The protection repository
     * 
     * @var ProtectionRepositoryInterface
     */
    protected $protection;

    /**
     * Create a new ProtectionSeeder instance.
     * 
     * @param ProtectionRepositoryInterface $protection
     */
    public function __construct(
        ProtectionRepositoryInterface $protection
    ) {
        $this->protection = $protection;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $pro = ["Top Rope", "Bolted", "Trad"];

        for ($i = 0; $i < sizeof($pro); $i++) {
            $this->protection->create(
                [
                    "name" => $pro[$i],
                    "slug" => str_slug($pro[$i], "-"),
                ]
            );
        }
    }
}
