<?php namespace Finnito\ClimbsModule\Protection;

use Finnito\ClimbsModule\Protection\Contract\ProtectionRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ProtectionRepository extends EntryRepository implements ProtectionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ProtectionModel
     */
    protected $model;

    /**
     * Create a new ProtectionRepository instance.
     *
     * @param ProtectionModel $model
     */
    public function __construct(ProtectionModel $model)
    {
        $this->model = $model;
    }
}
