<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Crag\Form\CragFormBuilder;
use Finnito\ClimbsModule\Crag\Table\CragTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class CragsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param CragTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CragTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param CragFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CragFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param CragFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CragFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
