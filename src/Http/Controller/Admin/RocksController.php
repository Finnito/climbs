<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Rock\Form\RockFormBuilder;
use Finnito\ClimbsModule\Rock\Table\RockTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class RocksController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param RockTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(RockTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param RockFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(RockFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param RockFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(RockFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
