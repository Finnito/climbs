<?php namespace Finnito\ClimbsModule\Rating\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface RatingRepositoryInterface extends EntryRepositoryInterface
{

}
