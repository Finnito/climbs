<?php

return [
    'countries' => [
        'read',
        'write',
        'delete',
    ],
    'climbing_grades' => [
        'read',
        'write',
        'delete',
    ],
    'crags' => [
        'read',
        'write',
        'delete',
    ],
    'styles' => [
        'read',
        'write',
        'delete',
    ],
    'walls' => [
        'read',
        'write',
        'delete',
    ],
    'rocks' => [
        'read',
        'write',
        'delete',
    ],
    'protection' => [
        'read',
        'write',
        'delete',
    ],
    'type' => [
        'read',
        'write',
        'delete',
    ],
    'ratings' => [
        'read',
        'write',
        'delete',
    ],
    'climbs' => [
        'read',
        'write',
        'delete',
    ],
    'sends' => [
        'read',
        'write',
        'delete',
    ],
    'comments' => [
        'read',
        'write',
        'delete',
    ],
];
