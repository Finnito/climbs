<?php namespace Finnito\ClimbsModule\ClimbingGrade;

use Finnito\ClimbsModule\ClimbingGrade\Contract\ClimbingGradeRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ClimbingGradeRepository extends EntryRepository implements ClimbingGradeRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ClimbingGradeModel
     */
    protected $model;

    /**
     * Create a new ClimbingGradeRepository instance.
     *
     * @param ClimbingGradeModel $model
     */
    public function __construct(ClimbingGradeModel $model)
    {
        $this->model = $model;
    }
}
