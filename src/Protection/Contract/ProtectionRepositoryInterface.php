<?php namespace Finnito\ClimbsModule\Protection\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ProtectionRepositoryInterface extends EntryRepositoryInterface
{

}
