<?php namespace Finnito\ClimbsModule\Style;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Finnito\ClimbsModule\Style\Contract\StyleRepositoryInterface;

class StyleSeeder extends Seeder
{

    /**
     * The style repository interface.
     * 
     * @var StyleRepositoryInterface
     */
    protected $styles;

    /**
     * Create a new StyleRepositoryInterface instance.
     * 
     * @param StyleRepositoryInterface $styles
     */
    public function __construct(
        StyleRepositoryInterface $styles
    ) {
        $this->styles = $styles;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $styles = ["Overhang", "Slab"];

        for ($i = 0; $i < sizeof($styles); $i++) {
            $this->styles->create(
                [
                    "name" => $styles[$i],
                    "slug" => str_slug($styles[$i], "-"),
                ]
            );
        }
    }
}
