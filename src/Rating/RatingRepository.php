<?php namespace Finnito\ClimbsModule\Rating;

use Finnito\ClimbsModule\Rating\Contract\RatingRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class RatingRepository extends EntryRepository implements RatingRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var RatingModel
     */
    protected $model;

    /**
     * Create a new RatingRepository instance.
     *
     * @param RatingModel $model
     */
    public function __construct(RatingModel $model)
    {
        $this->model = $model;
    }
}
