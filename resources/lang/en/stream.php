<?php

return [
    'countries' => [
        'name' => 'Countries',
    ],
    'climbing_grades' => [
        'name' => 'Climbing Grades',
    ],
    'crags' => [
        'name' => 'Crags',
    ],
    'styles' => [
        'name' => 'Styles',
    ],
    'walls' => [
        'name' => 'Walls',
    ],
    'rocks' => [
        'name' => 'Rocks',
    ],
    'protection' => [
        'name' => 'Protection',
    ],
    'type' => [
        'name' => 'Type',
    ],
    'ratings' => [
        'name' => 'Ratings',
    ],
    'climbs' => [
        'name' => 'Climbs',
    ],
    'sends' => [
        'name' => 'Sends',
    ],
    'comments' => [
        'name' => 'Comments',
    ],
];
