<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleClimbsCreateProtectionStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'protection',
        'title_column' => 'name',
        'translatable' => true,
        'versionable' => true,
        'trashable' => true,
        'searchable' => true,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "description",
    ];

}
