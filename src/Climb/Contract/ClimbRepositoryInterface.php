<?php namespace Finnito\ClimbsModule\Climb\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ClimbRepositoryInterface extends EntryRepositoryInterface
{

}
