<?php namespace Finnito\ClimbsModule\Send\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface SendRepositoryInterface extends EntryRepositoryInterface
{

}
