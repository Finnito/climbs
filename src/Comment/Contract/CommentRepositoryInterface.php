<?php namespace Finnito\ClimbsModule\Comment\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CommentRepositoryInterface extends EntryRepositoryInterface
{

}
