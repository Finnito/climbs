<?php

return [
    'countries' => [
        'name'   => 'Countries',
        'option' => [
            'read'   => 'Can read countries?',
            'write'  => 'Can create/edit countries?',
            'delete' => 'Can delete countries?',
        ],
    ],
    'climbing_grades' => [
        'name'   => 'Climbing Grades',
        'option' => [
            'read'   => 'Can read climbing grades?',
            'write'  => 'Can create/edit climbing grades?',
            'delete' => 'Can delete climbing grades?',
        ],
    ],
    'crags' => [
        'name'   => 'Crags',
        'option' => [
            'read'   => 'Can read crags?',
            'write'  => 'Can create/edit crags?',
            'delete' => 'Can delete crags?',
        ],
    ],
    'styles' => [
        'name'   => 'Styles',
        'option' => [
            'read'   => 'Can read styles?',
            'write'  => 'Can create/edit styles?',
            'delete' => 'Can delete styles?',
        ],
    ],
    'walls' => [
        'name'   => 'Walls',
        'option' => [
            'read'   => 'Can read walls?',
            'write'  => 'Can create/edit walls?',
            'delete' => 'Can delete walls?',
        ],
    ],
    'rocks' => [
        'name'   => 'Rocks',
        'option' => [
            'read'   => 'Can read rocks?',
            'write'  => 'Can create/edit rocks?',
            'delete' => 'Can delete rocks?',
        ],
    ],
    'protection' => [
        'name'   => 'Protection',
        'option' => [
            'read'   => 'Can read protection?',
            'write'  => 'Can create/edit protection?',
            'delete' => 'Can delete protection?',
        ],
    ],
    'type' => [
        'name'   => 'Type',
        'option' => [
            'read'   => 'Can read type?',
            'write'  => 'Can create/edit type?',
            'delete' => 'Can delete type?',
        ],
    ],
    'ratings' => [
        'name'   => 'Ratings',
        'option' => [
            'read'   => 'Can read ratings?',
            'write'  => 'Can create/edit ratings?',
            'delete' => 'Can delete ratings?',
        ],
    ],
    'climbs' => [
        'name'   => 'Climbs',
        'option' => [
            'read'   => 'Can read climbs?',
            'write'  => 'Can create/edit climbs?',
            'delete' => 'Can delete climbs?',
        ],
    ],
    'sends' => [
        'name'   => 'Sends',
        'option' => [
            'read'   => 'Can read sends?',
            'write'  => 'Can create/edit sends?',
            'delete' => 'Can delete sends?',
        ],
    ],
    'comments' => [
        'name'   => 'Comments',
        'option' => [
            'read'   => 'Can read comments?',
            'write'  => 'Can create/edit comments?',
            'delete' => 'Can delete comments?',
        ],
    ],
];
