<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\ClimbingGrade\Form\ClimbingGradeFormBuilder;
use Finnito\ClimbsModule\ClimbingGrade\Table\ClimbingGradeTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ClimbingGradesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ClimbingGradeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ClimbingGradeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ClimbingGradeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ClimbingGradeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ClimbingGradeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ClimbingGradeFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
