<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Climb\Form\ClimbFormBuilder;
use Finnito\ClimbsModule\Climb\Table\ClimbTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ClimbsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ClimbTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ClimbTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ClimbFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ClimbFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ClimbFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ClimbFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
