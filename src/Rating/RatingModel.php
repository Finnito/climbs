<?php namespace Finnito\ClimbsModule\Rating;

use Finnito\ClimbsModule\Rating\Contract\RatingInterface;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsRatingsEntryModel;

class RatingModel extends ClimbsRatingsEntryModel implements RatingInterface
{

}
