<?php namespace Finnito\ClimbsModule\ClimbingGrade;

use Finnito\ClimbsModule\ClimbingGrade\Contract\ClimbingGradeInterface;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsClimbingGradesEntryModel;

class ClimbingGradeModel extends ClimbsClimbingGradesEntryModel implements ClimbingGradeInterface
{

}
