<?php namespace Finnito\ClimbsModule\Climb;

use Finnito\ClimbsModule\Climb\Contract\ClimbRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ClimbRepository extends EntryRepository implements ClimbRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ClimbModel
     */
    protected $model;

    /**
     * Create a new ClimbRepository instance.
     *
     * @param ClimbModel $model
     */
    public function __construct(ClimbModel $model)
    {
        $this->model = $model;
    }
}
