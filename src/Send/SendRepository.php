<?php namespace Finnito\ClimbsModule\Send;

use Finnito\ClimbsModule\Send\Contract\SendRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class SendRepository extends EntryRepository implements SendRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var SendModel
     */
    protected $model;

    /**
     * Create a new SendRepository instance.
     *
     * @param SendModel $model
     */
    public function __construct(SendModel $model)
    {
        $this->model = $model;
    }
}
