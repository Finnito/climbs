<?php namespace Finnito\ClimbsModule\Rock\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface RockRepositoryInterface extends EntryRepositoryInterface
{

}
