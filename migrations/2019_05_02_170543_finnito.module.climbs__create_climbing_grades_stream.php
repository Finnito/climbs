<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleClimbsCreateClimbingGradesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'climbing_grades',
        'title_column' => 'aus-nz',
        'translatable' => false,
        'versionable' => true,
        'trashable' => true,
        'searchable' => true,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'yds' => [
            'unique' => true,
            'required' => true,
        ],
        "british",
        "french",
        "uiia",
        "saxon",
        "aus-nz",
        "south-africa",
        "finnish",
        "nor",
        "brazilian",
        "kurtyka",
    ];

}
