<?php namespace Finnito\ClimbsModule\Wall\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface WallRepositoryInterface extends EntryRepositoryInterface
{

}
