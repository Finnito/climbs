<?php namespace Finnito\ClimbsModule\Rock;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Finnito\ClimbsModule\Rock\Contract\RockRepositoryInterface;

class RockSeeder extends Seeder
{
    /**
     * The rock repository
     * 
     * @var RockRepositoryInterface
     */
    protected $rocks;

    /**
     * Create a new RockSeeder instance.
     * 
     * @param RockRepositoryInterface $rocks
     */
    public function __construct(
        RockRepositoryInterface $rocks
    ) {
        $this->rocks = $rocks;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $types = ["Limestone", "Obsidian"];

        for ($i = 0; $i < sizeof($types); $i++) {
            $this->rocks->create(
                [
                    "name" => $types[$i],
                    "slug" => str_slug($types[$i], "-"),
                ]
            );
        }
    }
}
