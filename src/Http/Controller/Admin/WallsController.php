<?php namespace Finnito\ClimbsModule\Http\Controller\Admin;

use Finnito\ClimbsModule\Wall\Form\WallFormBuilder;
use Finnito\ClimbsModule\Wall\Table\WallTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class WallsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param WallTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(WallTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param WallFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(WallFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param WallFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(WallFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
