<?php namespace Finnito\ClimbsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class ClimbsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-puzzle-piece';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'climbs' => [
            'buttons' => [
                'new_climb',
            ],
        ],
        'countries' => [
            'buttons' => [
                'new_country',
            ],
        ],
        'climbing_grades' => [
            'buttons' => [
                'new_climbing_grade',
            ],
        ],
        'crags' => [
            'buttons' => [
                'new_crag',
            ],
        ],
        'styles' => [
            'buttons' => [
                'new_style',
            ],
        ],
        'walls' => [
            'buttons' => [
                'new_wall',
            ],
        ],
        'rocks' => [
            'buttons' => [
                'new_rock',
            ],
        ],
        'protection' => [
            'buttons' => [
                'new_protection',
            ],
        ],
        'type' => [
            'buttons' => [
                'new_type',
            ],
        ],
        'ratings' => [
            'buttons' => [
                'new_rating',
            ],
        ],
        'sends' => [
            'buttons' => [
                'new_send',
            ],
        ],
        'comments' => [
            'buttons' => [
                'new_comment',
            ],
        ],
    ];

}
