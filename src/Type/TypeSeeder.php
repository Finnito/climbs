<?php namespace Finnito\ClimbsModule\Type;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Finnito\ClimbsModule\Type\Contract\TypeRepositoryInterface;

class TypeSeeder extends Seeder
{

    /**
     * The type repository
     * 
     * @var TypeRepositoryInterface
     */
    protected $types;

    /**
     * Create a new TypeSeeder instance/
     * 
     * @param TypeRepositoryInterface $types
     */
    public function __construct(
        TypeRepositoryInterface $types
    ) {
        $this->types = $types;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $types = ["Free Climbing", "Bouldering", "Aid"];

        for ($i = 0; $i < sizeof($types); $i++) {
            $this->types->create(
                [
                    "name" => $types[$i],
                    "slug" => str_slug($types[$i], "-"),
                ]
            );
        }
    }
}
