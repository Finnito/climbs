<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\ClimbsModule\Country\CountryModel;
use Finnito\ClimbsModule\Crag\CragModel;
use Finnito\ClimbsModule\Rock\RockModel;
use Finnito\ClimbsModule\Climb\ClimbModel;
use Finnito\ClimbsModule\ClimbingGrade\ClimbingGradeModel;
use Finnito\ClimbsModule\Protection\ProtectionModel;
use Finnito\ClimbsModule\Style\StyleModel;
use Finnito\ClimbsModule\Type\TypeModel;
use Finnito\ClimbsModule\Wall\WallModel;

class FinnitoModuleClimbsCreateClimbsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name' => 'anomaly.field_type.text',
        'slug' => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type' => '-'
            ],
        ],
        "wysiwyg" => [
            "type" => "anomaly.field_type.wysiwyg",
            "config" => [
                
            ]
        ],

        /**
         * Fields for stream: climbing_grades
         * Reference: https://en.wikipedia.org/wiki/Grade_(climbing)#Comparison_tables
         */
        "yds" => "anomaly.field_type.text",
        "british" => "anomaly.field_type.text",
        "french" => "anomaly.field_type.text",
        "uiia" => "anomaly.field_type.text",
        "saxon" => "anomaly.field_type.text",
        "aus-nz" => "anomaly.field_type.text",
        "south-africa" => "anomaly.field_type.text",
        "finnish" => "anomaly.field_type.text",
        "nor" => "anomaly.field_type.text",
        "brazilian" => "anomaly.field_type.text",
        "kurtyka" => "anomaly.field_type.text", // Polish

        /**
         * Fields for stream: crags
         * 
         * Uses: name, slug
         */
        "country" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => CountryModel::class,
                "mode" => "search",

            ]
        ],
        "rock" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => RockModel::class,
                "mode" => "search",
            ]
        ],

        /**
         * Fields for stream: walls
         * 
         * Uses: name, slug
         */
        "crag" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => CragModel::class,
                "mode" => "search",
            ]
        ],

        /**
         * Fields for stream: protection
         * 
         * Uses: name, slug
         */
        "description" => "anomaly.field_type.wysiwyg",

        /**
         * Fields for stream: ratings
         * 
         * Uses: created_by
         */
        "rating" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "default_value" => null,
                "separator" => "",
                "min" => 0,
                "max" => 3,
            ]
        ],
        "climb" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => ClimbModel::class,
                "mode" => "search",
            ]
        ],

        /**
         * Fields for stream: climbs
         * Uses: name, slug
         */
        "grade" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => ClimbingGradeModel::class,
                "mode" => "search",
            ],
        ],
        "protection" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => ProtectionModel::class,
                "mode" => "search",
            ]
        ],
        "protection_description" => [
            "type" => "anomaly.field_type.text",
        ],
        "style" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => StyleModel::class,
                "mode" => "search",
            ]
        ],
        "type" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => TypeModel::class,
                "mode" => "search",
            ]
        ],
        "first_ascent" => "anomaly.field_type.text",
        "first_ascent_year" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => "",
            ]
        ],
        "beta" => [
            "type" => "anomaly.field_type.wysiwyg",
            "config" => [

            ]
        ],
        "coordinates" => [
            "type" => "anomaly.field_type.geocoder",
            "config" => [

            ]
        ],
        "wall" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => WallModel::class,
                "mode" => "search",
            ]
        ],
    ];

}
