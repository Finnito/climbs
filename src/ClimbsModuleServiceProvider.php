<?php namespace Finnito\ClimbsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\ClimbsModule\Comment\Contract\CommentRepositoryInterface;
use Finnito\ClimbsModule\Comment\CommentRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsCommentsEntryModel;
use Finnito\ClimbsModule\Comment\CommentModel;
use Finnito\ClimbsModule\Send\Contract\SendRepositoryInterface;
use Finnito\ClimbsModule\Send\SendRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsSendsEntryModel;
use Finnito\ClimbsModule\Send\SendModel;
use Finnito\ClimbsModule\Climb\Contract\ClimbRepositoryInterface;
use Finnito\ClimbsModule\Climb\ClimbRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsClimbsEntryModel;
use Finnito\ClimbsModule\Climb\ClimbModel;
use Finnito\ClimbsModule\Rating\Contract\RatingRepositoryInterface;
use Finnito\ClimbsModule\Rating\RatingRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsRatingsEntryModel;
use Finnito\ClimbsModule\Rating\RatingModel;
use Finnito\ClimbsModule\Type\Contract\TypeRepositoryInterface;
use Finnito\ClimbsModule\Type\TypeRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsTypeEntryModel;
use Finnito\ClimbsModule\Type\TypeModel;
use Finnito\ClimbsModule\Protection\Contract\ProtectionRepositoryInterface;
use Finnito\ClimbsModule\Protection\ProtectionRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsProtectionEntryModel;
use Finnito\ClimbsModule\Protection\ProtectionModel;
use Finnito\ClimbsModule\Rock\Contract\RockRepositoryInterface;
use Finnito\ClimbsModule\Rock\RockRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsRocksEntryModel;
use Finnito\ClimbsModule\Rock\RockModel;
use Finnito\ClimbsModule\Wall\Contract\WallRepositoryInterface;
use Finnito\ClimbsModule\Wall\WallRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsWallsEntryModel;
use Finnito\ClimbsModule\Wall\WallModel;
use Finnito\ClimbsModule\Style\Contract\StyleRepositoryInterface;
use Finnito\ClimbsModule\Style\StyleRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsStylesEntryModel;
use Finnito\ClimbsModule\Style\StyleModel;
use Finnito\ClimbsModule\Crag\Contract\CragRepositoryInterface;
use Finnito\ClimbsModule\Crag\CragRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsCragsEntryModel;
use Finnito\ClimbsModule\Crag\CragModel;
use Finnito\ClimbsModule\ClimbingGrade\Contract\ClimbingGradeRepositoryInterface;
use Finnito\ClimbsModule\ClimbingGrade\ClimbingGradeRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsClimbingGradesEntryModel;
use Finnito\ClimbsModule\ClimbingGrade\ClimbingGradeModel;
use Finnito\ClimbsModule\Country\Contract\CountryRepositoryInterface;
use Finnito\ClimbsModule\Country\CountryRepository;
use Anomaly\Streams\Platform\Model\Climbs\ClimbsCountriesEntryModel;
use Finnito\ClimbsModule\Country\CountryModel;
use Finnito\ClimbsModule\Http\Controller\Admin\ClimbVersionsController;
use Anomaly\Streams\Platform\Version\VersionRouter;
use Illuminate\Routing\Router;

class ClimbsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/climbs/comments'           => 'Finnito\ClimbsModule\Http\Controller\Admin\CommentsController@index',
        'admin/climbs/comments/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\CommentsController@create',
        'admin/climbs/comments/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\CommentsController@edit',
        'admin/climbs/sends'           => 'Finnito\ClimbsModule\Http\Controller\Admin\SendsController@index',
        'admin/climbs/sends/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\SendsController@create',
        'admin/climbs/sends/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\SendsController@edit',
        'admin/climbs'           => 'Finnito\ClimbsModule\Http\Controller\Admin\ClimbsController@index',
        'admin/climbs/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\ClimbsController@create',
        'admin/climbs/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\ClimbsController@edit',
        'admin/climbs/ratings'           => 'Finnito\ClimbsModule\Http\Controller\Admin\RatingsController@index',
        'admin/climbs/ratings/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\RatingsController@create',
        'admin/climbs/ratings/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\RatingsController@edit',
        'admin/climbs/type'           => 'Finnito\ClimbsModule\Http\Controller\Admin\TypeController@index',
        'admin/climbs/type/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\TypeController@create',
        'admin/climbs/type/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\TypeController@edit',
        'admin/climbs/protection'           => 'Finnito\ClimbsModule\Http\Controller\Admin\ProtectionController@index',
        'admin/climbs/protection/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\ProtectionController@create',
        'admin/climbs/protection/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\ProtectionController@edit',
        'admin/climbs/rocks'           => 'Finnito\ClimbsModule\Http\Controller\Admin\RocksController@index',
        'admin/climbs/rocks/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\RocksController@create',
        'admin/climbs/rocks/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\RocksController@edit',
        'admin/climbs/walls'           => 'Finnito\ClimbsModule\Http\Controller\Admin\WallsController@index',
        'admin/climbs/walls/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\WallsController@create',
        'admin/climbs/walls/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\WallsController@edit',
        'admin/climbs/styles'           => 'Finnito\ClimbsModule\Http\Controller\Admin\StylesController@index',
        'admin/climbs/styles/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\StylesController@create',
        'admin/climbs/styles/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\StylesController@edit',
        'admin/climbs/crags'           => 'Finnito\ClimbsModule\Http\Controller\Admin\CragsController@index',
        'admin/climbs/crags/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\CragsController@create',
        'admin/climbs/crags/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\CragsController@edit',
        'admin/climbs/climbing_grades'           => 'Finnito\ClimbsModule\Http\Controller\Admin\ClimbingGradesController@index',
        'admin/climbs/climbing_grades/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\ClimbingGradesController@create',
        'admin/climbs/climbing_grades/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\ClimbingGradesController@edit',
        'admin/climbs/countries'           => 'Finnito\ClimbsModule\Http\Controller\Admin\CountriesController@index',
        'admin/climbs/countries/create'    => 'Finnito\ClimbsModule\Http\Controller\Admin\CountriesController@create',
        'admin/climbs/countries/edit/{id}' => 'Finnito\ClimbsModule\Http\Controller\Admin\CountriesController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\ClimbsModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\ClimbsModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\ClimbsModule\Event\ExampleEvent::class => [
        //    Finnito\ClimbsModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\ClimbsModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        ClimbsCommentsEntryModel::class => CommentModel::class,
        ClimbsSendsEntryModel::class => SendModel::class,
        ClimbsClimbsEntryModel::class => ClimbModel::class,
        ClimbsRatingsEntryModel::class => RatingModel::class,
        ClimbsTypeEntryModel::class => TypeModel::class,
        ClimbsProtectionEntryModel::class => ProtectionModel::class,
        ClimbsRocksEntryModel::class => RockModel::class,
        ClimbsWallsEntryModel::class => WallModel::class,
        ClimbsStylesEntryModel::class => StyleModel::class,
        ClimbsCragsEntryModel::class => CragModel::class,
        ClimbsClimbingGradesEntryModel::class => ClimbingGradeModel::class,
        ClimbsCountriesEntryModel::class => CountryModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        CommentRepositoryInterface::class => CommentRepository::class,
        SendRepositoryInterface::class => SendRepository::class,
        ClimbRepositoryInterface::class => ClimbRepository::class,
        RatingRepositoryInterface::class => RatingRepository::class,
        TypeRepositoryInterface::class => TypeRepository::class,
        ProtectionRepositoryInterface::class => ProtectionRepository::class,
        RockRepositoryInterface::class => RockRepository::class,
        WallRepositoryInterface::class => WallRepository::class,
        StyleRepositoryInterface::class => StyleRepository::class,
        CragRepositoryInterface::class => CragRepository::class,
        ClimbingGradeRepositoryInterface::class => ClimbingGradeRepository::class,
        CountryRepositoryInterface::class => CountryRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router, VersionRouter $versions)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
        $versions->route($this->addon, ClimbVersionsController::class);
    }

}
