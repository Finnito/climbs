<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleClimbsCreateClimbsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'climbs',
        'title_column' => 'name',
        'translatable' => true,
        'versionable' => true,
        'trashable' => true,
        'searchable' => true,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "grade",
        "protection",
        "protection_description",
        "style",
        "type",
        "first_ascent",
        "first_ascent_year",
        "description",
        "beta",
        "coordinates",
        "wall",
        "crag",
    ];

}
