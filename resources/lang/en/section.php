<?php

return [
    'countries' => [
        'title' => 'Countries',
    ],
    'climbing_grades' => [
        'title' => 'Climbing Grades',
    ],
    'crags' => [
        'title' => 'Crags',
    ],
    'styles' => [
        'title' => 'Styles',
    ],
    'walls' => [
        'title' => 'Walls',
    ],
    'rocks' => [
        'title' => 'Rocks',
    ],
    'protection' => [
        'title' => 'Protection',
    ],
    'type' => [
        'title' => 'Type',
    ],
    'ratings' => [
        'title' => 'Ratings',
    ],
    'climbs' => [
        'title' => 'Climbs',
    ],
    'sends' => [
        'title' => 'Sends',
    ],
    'comments' => [
        'title' => 'Comments',
    ],
];
