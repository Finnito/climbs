<?php namespace Finnito\ClimbsModule\ClimbingGrade\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ClimbingGradeRepositoryInterface extends EntryRepositoryInterface
{

}
