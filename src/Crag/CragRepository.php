<?php namespace Finnito\ClimbsModule\Crag;

use Finnito\ClimbsModule\Crag\Contract\CragRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class CragRepository extends EntryRepository implements CragRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var CragModel
     */
    protected $model;

    /**
     * Create a new CragRepository instance.
     *
     * @param CragModel $model
     */
    public function __construct(CragModel $model)
    {
        $this->model = $model;
    }
}
